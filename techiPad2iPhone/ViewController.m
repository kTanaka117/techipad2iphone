//
//  ViewController.m
//  techiPad2iPhone
//
//  Created by 田中 賢治 on 2014/01/12.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    labelFrag = FALSE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)action:(id)sender{
    
    if (labelFrag)
    {
        self.label.text = @" ";
        labelFrag = FALSE;
    }else{
        self.label.text = @"こんにちは";
        labelFrag = TRUE;
    }
}

@end
