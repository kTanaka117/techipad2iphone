//
//  main.m
//  techiPad2iPhone
//
//  Created by 田中 賢治 on 2014/01/12.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
