//
//  ViewController.h
//  techiPad2iPhone
//
//  Created by 田中 賢治 on 2014/01/12.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    BOOL labelFrag;
}

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
